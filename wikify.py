import sys, getopt, urllib2, urllib

API_URL = 'http://wikipedia-miner.cms.waikato.ac.nz/services/wikify?'
DOCUMENTATION_URL = 'http://wikipedia-miner.cms.waikato.ac.nz/services/?wikify'

# parameter that specifies the file that is going to be wikified
FILE_PATH = 'filePath'

SOURCE = 'source'
SOURCE_MODE = 'sourceMode'
MIN_PROBABILITY = 'minProbability'
REPEAT_MODE = 'repeatMode'
TOPICS = 'topics'
RESPONSE_FORMAT = 'responseFormat'

# call the service and return the results
def callService(url):
	request = urllib2.Request(url)
	response = urllib2.urlopen(request)
	response = response.read();
	return response

# print the instructions on how to use the parameters
def printInstructions():
	print 'USAGE: wikify.py -f <%s> [-s <%s> -m <%s> -r <%s> -t <%s>, -e <%s>]' % (FILE_PATH, SOURCE_MODE, MIN_PROBABILITY, REPEAT_MODE, TOPICS, RESPONSE_FORMAT)
	print "Possible values of arguments are defined in wikiminer's documentation at %s" % (DOCUMENTATION_URL)

# exit if no input file was passed
if len(sys.argv) == 1:
	printInstructions()
	sys.exit()
	
try: 
	opts, args = getopt.getopt(sys.argv[1:], 'f:s:m:r:t:e:', [FILE_PATH, SOURCE_MODE, MIN_PROBABILITY, REPEAT_MODE, TOPICS, RESPONSE_FORMAT])

	# create an empty dictionary to hold url parameters
	parameters = {}
	for o, a in opts:
		if o in ('-f', FILE_PATH):
			f = open(a, 'r')
			source = f.read()
			parameters[SOURCE] = source
		elif o in ('-s', SOURCE_MODE):
			sourceMode = a
			parameters[SOURCE_MODE] = a
		elif o in ('-m', MIN_PROBABILITY):
			parameters[MIN_PROBABILITY] = a
		elif o in ('-r', REPEAT_MODE):
			parameters[REPEAT_MODE] = a
		elif o in ('-t', TOPICS):
			parameters[TOPICS] = a
		elif o in ('-e', RESPONSE_FORMAT):
			parameters[RESPONSE_FORMAT] = a
		
	# encode the service url using the parameters
	encodedParameters = urllib.urlencode(parameters)
	url = API_URL + encodedParameters

	response = callService(url)

	print response

except getopt.GetoptError as err:
	print str(err)
	printInstructions()
